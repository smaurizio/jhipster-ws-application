/**
 * Data Access Objects used by WebSocket services.
 */
package io.github.demo.application.web.websocket.dto;
