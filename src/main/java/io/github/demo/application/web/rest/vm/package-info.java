/**
 * View Models used by Spring MVC REST controllers.
 */
package io.github.demo.application.web.rest.vm;
