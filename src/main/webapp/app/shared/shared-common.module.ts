import { NgModule } from '@angular/core';

import { WsApplicationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [WsApplicationSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [WsApplicationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class WsApplicationSharedCommonModule {}
